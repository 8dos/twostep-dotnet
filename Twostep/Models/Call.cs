﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace Twostep.Models
{
    public class Call
    {
        [JsonProperty("ignored")]
        public bool Ignored { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }
        
        public Call()
        {
        }

        public static Call FromJSON(string json)
        {
            JObject obj = JObject.Parse(json);
            var data = obj["data"].ToString();
            return JsonConvert.DeserializeObject<Call>(data);
        }
    }
}
