﻿using System;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace Twostep.Models
{
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        public Token()
        {
        }

        public static Token FromJSON(string json)
        {
            return JsonConvert.DeserializeObject<Token>(json);
        }

        public bool IsValid()
        {
            return !(string.IsNullOrEmpty(this.AccessToken) && string.IsNullOrEmpty(this.RefreshToken));
        }

        public bool IsExpired()
        {
            try
            {
                var decoded = new JwtSecurityToken(this.AccessToken);
                if (decoded != null)
                {
                    var exp = decoded.Payload.Exp;
                    var now = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    if (exp > now) {
                        return false; // not expired
                    } 
                }
            }
            catch (ArgumentException)
            {
                // Invalid token
            }
            return true; // invalid or expired
        }
    }
}
