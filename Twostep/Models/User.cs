﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace Twostep.Models
{
    public class User
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("country_code")]
        public int CountryCode { get; set; }

        public User()
        {
        }

        public static User FromJSON(string json)
        {
            JObject obj = JObject.Parse(json);
            var data = obj["data"].ToString();
            return JsonConvert.DeserializeObject<User>(data);

        }
    }
}
