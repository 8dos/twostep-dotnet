using Twostep.Models;

namespace Twostep.Models
{
    public interface ITokenStore
    {
        Token Load();

        void Save(Token token);

        void Clear();
    }
}