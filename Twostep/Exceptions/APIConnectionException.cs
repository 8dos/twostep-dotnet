namespace Twostep.Exceptions
{
    public class APIConnectionException : APIException
    {
        public APIConnectionException(string message) : base(message)
        {
        }
    }
}