namespace Twostep
{
    public class Constants
    {
        ///
        /// Client version.
        ///
        public static string VERSION = "0.6.1";

        ///
        /// Live API URL
        ///
        public const string LIVE_API_URL = "https://api.twostep.io";

        ///
        /// Sandbox API URL
        /// 
        public const string SANDBOX_API_URL = "http://localhost:4000"; // "https://sandbox-api.twostep.io";

        ///
        /// Default timeout is 30 seconds.
        ///
        public const int DEFAULT_TIMEOUT = 30; // in seconds
    }
}
