﻿using System;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json.Linq;

using Twostep.Models;

namespace Twostep
{
    public partial class Client : AbstractClient
    {
        ///
        /// Constructor. 
        ///
        public Client(string clientId, string clientSecret, bool sandbox = false) : base(clientId, clientSecret, sandbox) {}
        
        ///
        /// Create new user.
        ///
        public async Task<User> CreateUser(string email, string phone, int countryCode, bool force = false)
        {
            JObject payload = new JObject();
            payload.Add("email", email);
            payload.Add("phone", phone);
            payload.Add("country_code", countryCode);

            var response = await Post("/v1/users", payload);
            return User.FromJSON(response);
        }

        ///
        /// Get user status.
        ///
        public async Task<User> GetUser(string userId)
        {
            var response = await Get("/v1/users/" + Uri.EscapeDataString(userId));
            return User.FromJSON(response);
        }

        ///
        /// Request sms message.
        ///
        public async Task<Sms> RequestSms(string userId, bool force = false)
        {
            JObject payload = new JObject();
            payload.Add("force", force);

            var response = await Post("/v1/users/" + Uri.EscapeDataString(userId) + "/sms", payload);
            return Sms.FromJSON(response);
        }

        ///
        /// Request phone call.
        ///
        public async Task<Call> RequestCall(string userId, bool force = false)
        {
            JObject payload = new JObject();
            payload.Add("force", force);

            var response = await Post("/v1/users/" + Uri.EscapeDataString(userId) + "/call", payload);
            return Call.FromJSON(response);
        }

        ///
        /// Remove user.
        ///
        public async Task<User> RemoveUser(string userId)
        {
            var response = await Delete("/v1/users/" + Uri.EscapeDataString(userId));
            return User.FromJSON(response);
        }
    }
}
