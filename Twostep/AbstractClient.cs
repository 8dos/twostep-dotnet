using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

using Twostep.Exceptions;
using Twostep.Models;

namespace Twostep
{
    public abstract class AbstractClient
    {
        #region --- Properties ---        

        protected int _timeout = Constants.DEFAULT_TIMEOUT;

        public string ClientId { get; private set; }
        public string ClientSecret { get; private set; }

        public ITokenStore TokenStore { get; set; }

        public string ApiUrl { get; set; }
        public int Timeout
        {
            get { return _timeout; }
            set
            {
                if (value < 1 || value > 180)
                {
                    throw new ArgumentException("Timeout must be within 1 and 180 seconds");
                }
                _timeout = value;
            }
        }
        public string UserAgent { get; private set; }

        #endregion


        ///
        /// Constructor.
        ///
        protected AbstractClient(string clientId, string clientSecret, bool sandbox = false)
        {
            this.ApiUrl = sandbox ? Constants.SANDBOX_API_URL : Constants.LIVE_API_URL;
            this.UserAgent = "twostep-dotnet/" + Constants.VERSION;

            this.ClientId = clientId;
            this.ClientSecret = clientSecret;

            if (string.IsNullOrEmpty(this.ClientId))
            {
                throw new ArgumentException("No Client ID provided");
            }
            if (string.IsNullOrEmpty(this.ClientSecret))
            {
                throw new ArgumentException("No Client secret provided");
            }
        }


        protected async Task<string> Get(string path)
        {
            return await Request("GET", path);
        }

        protected async Task<string> Post(string path, JObject payload)
        {
            return await Request("POST", path, payload);
        }

        protected async Task<string> Put(string path, JObject payload)
        {
            return await Request("PUT", path, payload);
        }

        protected async Task<string> Delete(string path)
        {
            return await Request("DELETE", path);
        }

        protected async Task<string> Request(string method, string path, JObject data = null)
        {
            try
            {
                var token = await GetToken();

                var client = new HttpClient();
                client.Timeout = new TimeSpan(0, 0, this.Timeout);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("User-Agent", this.UserAgent);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.AccessToken);

                StringContent payload = null;
                if (data != null)
                {
                    payload = new StringContent(data.ToString(Formatting.None), Encoding.UTF8, "application/json");
                }

                HttpResponseMessage response = null;
                switch (method)
                {
                    case "GET":
                        response = await client.GetAsync(this.ApiUrl + path);
                        break;
                    case "POST":
                        response = await client.PostAsync(this.ApiUrl + path, payload);
                        break;
                    case "PUT":
                        response = await client.PutAsync(this.ApiUrl + path, payload);
                        break;
                    case "DELETE":
                        response = await client.DeleteAsync(this.ApiUrl + path);
                        break;
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    if (response.Content != null)
                    {
                        response.Content.Dispose();
                    }
                    return content;
                }
                else
                {
                    int statusCode = (int)response.StatusCode;
                    var content = await response.Content.ReadAsStringAsync();
                    if (response.Content != null)
                    {
                        response.Content.Dispose();
                    }
                    throw new ResponseException(Error.FromJSON(content));
                }
            }
            catch (HttpRequestException ex)
            {
                throw new APIConnectionException(ex.Message);
            }
            catch (TaskCanceledException ex)
            {
                throw new APITimeoutException(ex.Message);
            }
        }

        protected async Task<Token> GetToken()
        {
            Token token = null;
            if (this.TokenStore != null)
            {
                token = this.TokenStore.Load();
            }

            if (token != null && token.IsValid())
            {
                if (token.IsExpired())
                {
                    return await RefreshToken(token);
                }
                return token;
            }

            return await NewToken();
        }

        protected async Task<Token> NewToken()
        {
            JObject payload = new JObject();
            payload.Add("client_id", this.ClientId);
            payload.Add("client_secret", this.ClientSecret);
            payload.Add("grant_type", "client_credentials");
            var s = payload.ToString(Formatting.None);
            var response = await AuthRequest(payload);
            var token = Token.FromJSON(response);

            if (this.TokenStore != null)
            {
                this.TokenStore.Save(token);
            }

            return token;
        }

        protected async Task<Token> RefreshToken(Token oldToken)
        {
            JObject payload = new JObject();
            payload.Add("client_id", this.ClientId);
            payload.Add("client_secret", this.ClientSecret);
            payload.Add("grant_type", "refresh_token");
            payload.Add("refresh_token", oldToken.RefreshToken);
            var s = payload.ToString(Formatting.None);
            var response = await AuthRequest(payload);
            var token = Token.FromJSON(response);

            if (this.TokenStore != null)
            {
                this.TokenStore.Save(token);
            }

            return token;
        }

        protected async Task<string> AuthRequest(JObject data)
        {
            try
            {
                var client = new HttpClient();
                client.Timeout = new TimeSpan(0, 0, this.Timeout);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("User-Agent", this.UserAgent);

                StringContent payload = new StringContent(data.ToString(Formatting.None), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(this.ApiUrl + "/auth/token", payload);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    if (response.Content != null)
                    {
                        response.Content.Dispose();
                    }
                    return content;
                }
                else
                {
                    int statusCode = (int)response.StatusCode;
                    var content = await response.Content.ReadAsStringAsync();
                    if (response.Content != null)
                    {
                        response.Content.Dispose();
                    }
                    throw new ResponseException(Error.FromJSON(content));
                }
            }
            catch (HttpRequestException ex)
            {
                throw new APIConnectionException(ex.Message);
            }
            catch (TaskCanceledException ex)
            {
                throw new APITimeoutException(ex.Message);
            }
        }

    }
}
