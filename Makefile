.PHONY: test build pack

test:
	dotnet test ./Twostep.Tests/

build:
	dotnet build ./Twostep/

pack:
	dotnet pack ./Twostep/ 

