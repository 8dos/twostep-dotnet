using System;
using Twostep;
using Twostep.Models;

namespace Twostep.Tests
{
    public class MyTokenStore : ITokenStore
    {
        private Token token = null;

        public Token Load()
        {
            Console.WriteLine("Loading token from store");
            return this.token;
        }

        public void Save(Token token)
        {
            Console.WriteLine("Saving token to store");
            this.token = token;
        }

        public void Clear()
        {
            this.token = null;
        }
    }
}