using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Net;
using System.Net.Security;
using System.Net.Http;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Twostep;
using Twostep.Exceptions;

namespace Twostep.Tests
{
    [TestClass]
    public class TestUsers
    {
        const string CLIENT_ID = "12345";
        const string CLIENT_SECRET = "67890";

        Client client = null;

        [TestInitialize]
        public void SetUp()
        {
            client = new Client(CLIENT_ID, CLIENT_SECRET, true);
            client.ApiUrl = "http://localhost:4000";
            client.TokenStore = new MyTokenStore();
            client.Timeout = 5;
        }

        [TestMethod]
        public void Timeout()
        {
            var cli = new Client(CLIENT_ID, CLIENT_SECRET, true);
            cli.Timeout = 5;
            Assert.AreEqual(5, client.Timeout);

            //Console.WriteLine("VERSION: " + client.UserAgent);
            //Assert.Fail();

            try
            {
               cli.Timeout = 5000;
               Assert.Fail();
            }
            catch (ArgumentException ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [TestMethod]
        public async Task CreateUser()
        {
            var user = await client.CreateUser("jdoe@email.com", "+12125551234", 1);
            Assert.IsNotNull(user);
        }

        [TestMethod]
        public async Task GetUser()
        {
            try
            {
                var user = await client.GetUser("123456");
                Assert.IsNotNull(user);

                await client.GetUser("000000");
                Assert.Fail();
            }
            catch (ResponseException ex)
            {
                Assert.AreEqual(404, ex.Status);
            }
            catch (APIConnectionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [TestMethod]
        public async Task RequestSms()
        {
            var sms = await client.RequestSms("123456");
            Assert.IsNotNull(sms);
        }

        [TestMethod]
        public async Task RequestCall()
        {
            var call = await client.RequestCall("123456");
            Assert.IsNotNull(call);
        }

        [TestMethod]
        public async Task RemoveUser()
        {
            var user = await client.RemoveUser("123456");
            Assert.IsNotNull(user);
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}
